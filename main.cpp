#include <complex>
#include <cstdlib>
#include <iostream>

using namespace std;

template <typename T>
class range
{
private:
    T _a;
    T _b;
public:
    range(double a, double b)
    {
        _a = a;
        _b = b;
    }
    range operator/(T divider)
    {
        return range(_a / divider, _b / divider);
    }
    char *str()
    {
        char *a = new char[40];
        sprintf(a, "(%.0lf;%.0lf)", _a, _b);
        return a;
    }
};

template <class TC>
class Matrix
{
private:
    TC*** values;
    int size;

public:
    Matrix(int n)
    {
        size = n;
        values = new TC**[size];

        for(int i = 0; i < size; i+=1)
        {
            values[i] = new TC*[size];
        }
    }

    void matrix_printf();

    void matrix_gen()
    {
        for(int i = 0; i < size; i+=1)
        {
            for(int j = 0; j < size; j+=1)
            {
                values[i][j] = new TC(rand() % 100, rand() % 100);
            }
        }
    }

    Matrix operator/(double divider)
    {
        Matrix * result = new Matrix(size);
        for(int i = 0; i < size; i+=1)
        {
            for(int j = 0; j < size; j+=1)
            {
                result->values[i][j] = new TC(*(values[i][j]) / divider);
            }
        }

        return *result;
    }
};

template <>
void Matrix<range<double>>::matrix_printf()
{
    for(int i = 0; i < size; i+=1)
    {
        for(int j = 0; j < size; j+=1)
        {
            cout << values[i][j]->str() << ' ';
        }
        cout << endl;
    }
}

template <>
void Matrix<complex<double>>::matrix_printf()
{
    for(int i = 0; i < size; i+=1)
    {
        for(int j = 0; j < size; j+=1)
        {
            if (values[i][j]->imag() > 0)
            {
                printf("%.0lf + %.0lfi ", values[i][j]->real(), values[i][j]->imag());
            }
            else
            {
                printf("%.0lf - %.0lfi ", values[i][j]->real(), - values[i][j]->imag());
            }
        }
        cout << endl;
    }
}

int main()
{
    cout << "Матрица комплексных чисел" << endl;
    Matrix<complex<double>> complex_matrix(6);
    complex_matrix.matrix_gen();
    complex_matrix.matrix_printf();

    cout << "Пополам:" << endl;
    (complex_matrix / 2).matrix_printf();

    cout << "Матрица интервалов" << endl;
    Matrix<range<double>> range_matrix(6);
    range_matrix.matrix_gen();
    range_matrix.matrix_printf();

    cout << "Пополам:" << endl;
    (range_matrix / 2).matrix_printf();
}
